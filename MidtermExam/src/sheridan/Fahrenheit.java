package sheridan;

public class Fahrenheit {
	public static void main (String args[]) {//
		System.out.println(convertFromCelsius(0));
	}
	static int convertFromCelsius(int celsius) {
		return (int) (Math.ceil(celsius*1.8)+32);
	}
}
