package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testRegular() {
		assertTrue("-40C should equal -40F",Fahrenheit.convertFromCelsius(-40)==-40);
	}
	@Test
	public void testExceptional() {//
		assertFalse("10C should not equal 0F",Fahrenheit.convertFromCelsius(10)==0);
	}
	@Test
	public void testboundaryIn() {
		assertTrue("1C should equal 34F",Fahrenheit.convertFromCelsius(1)==34);
	}
	@Test
	public void testboundaryOut() {
		assertTrue("-1C should equal 31F",Fahrenheit.convertFromCelsius(-1)==31);
	}

}
